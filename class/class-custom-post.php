<?php
/*
 * Our custom post type
 */
function portfolios_custom_init() {
  $labels = array(
    'name' => _x('Portfolios', 'post type general name'),
    'singular_name' => _x('Portfolio', 'post type singular name'),
    'add_new' => _x('Add New', 'Portfolio'),
    'add_new_item' => __('Add New Portfolio'),
    'edit_item' => __('Edit Portfolio'),
    'new_item' => __('New Portfolio'),
    'all_items' => __('All Portfolios'),
    'view_item' => __('View Portfolio'),
    'search_items' => __('Search Portfolios'),
    'not_found' =>  __('No Portfolios found'),
    'not_found_in_trash' => __('No Portfolios found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => __('Portfolios')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => array('slug'=>'portfolios'),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => 100,
    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
  ); 
  
  // Register the custom post type with the settings specified in the arrays
  register_post_type('portfolios',$args);
}

add_action( 'init', 'portfolios_custom_init' );