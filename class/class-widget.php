<?php
/*
 * Widget for our custom post type
 */
// Creating the sidebar widget 
class sidebar_widget extends WP_Widget {
    
    function __construct() {
        parent::__construct(
            // Base ID of widget
            'test_portfolios', 
            
            // Widget name will appear in UI
            __('Portfolio Widget', 'test_portfolios_domain'), 
            
            // Widget description
            array( 'description' => __( 'Widget displaying list of portfolios', 'test_portfolios_domain' ), ) 
            );
    }
    
    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        $posts_limit = $instance['posts_limit'];
        
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
        echo $args['before_title'] . $title . $args['after_title'];
        
        // This is where the code runs and output is displayed
        query_posts( array( 'post_type' => 'portfolios', 'posts_per_page' => $posts_limit ) );
        if ( have_posts() ) : while ( have_posts() ) : 
          
            the_post();
              
            ?>
            <h3><?php the_title(); ?></h3>
            <?php echo get_the_post_thumbnail(null, 'thumbnail'); ?>
            <p>
            <?php echo get_the_excerpt(); ?>
            </p>
              
            <?php 
            endwhile; endif; 
            wp_reset_query();        
            
            echo $args['after_widget'];
        }
            
    // Widget Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'test_portfolios_domain' );
        }
        if ( isset( $instance[ 'posts_limit' ] ) ) {
            $posts_limit = $instance[ 'posts_limit' ];
        }
        else {
            $posts_limit = 3;
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
            <input class="widefat" 
                id="<?php echo $this->get_field_id( 'title' ); ?>" 
                name="<?php echo $this->get_field_name( 'title' ); ?>" 
                type="text" 
                value="<?php echo esc_attr( $title ); ?>" />
            
            <label for="<?php echo $this->get_field_id( 'posts_limit' ); ?>"><?php _e( 'Posts limit:' ); ?></label> 
            <input class="widefat" 
                id="<?php echo $this->get_field_id( 'posts_limit' ); ?>" 
                name="<?php echo $this->get_field_name( 'posts_limit' ); ?>" 
                type="text" 
                value="<?php echo esc_attr( $posts_limit ); ?>" />
        </p>
        <?php 
    }
        
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['posts_limit'] = ( ! empty( $new_instance['posts_limit'] ) ) ? strip_tags( $new_instance['posts_limit'] ) : '';
        return $instance;
    }
} // Class sidebar_widget ends here

// Register and load the widget
function load_widget() {
    register_widget( 'sidebar_widget' );
}
add_action( 'widgets_init', 'load_widget' );